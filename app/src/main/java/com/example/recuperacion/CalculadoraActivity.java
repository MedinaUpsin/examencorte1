package com.example.recuperacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView lblUser;
    private EditText txtNum1;
    private EditText txtNum2;
    private TextView lblRes;
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;
    private Calculadora clc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        lblUser = (TextView) findViewById(R.id.lblUser);
        lblRes = (TextView) findViewById(R.id.lblResultado);
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMultiplica);
        btnDividir = (Button) findViewById(R.id.btnDivide);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        clc = new Calculadora();

        Bundle datos = getIntent().getExtras();
        String user = datos.getString("usuario");
        lblUser.setText("Usuario " + user);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el primer numero",Toast.LENGTH_SHORT).show();
                }
                else if(num2.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el segundo numero",Toast.LENGTH_SHORT).show();
                }
                else{
                    float n1 = Float.parseFloat(num1);
                    float n2 = Float.parseFloat(num2);
                    clc.setNum1(n1);
                    clc.setNum2(n2);
                    String resultado = String.valueOf(clc.suma());
                    lblRes.setText(resultado);
                }
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el primer numero",Toast.LENGTH_SHORT).show();
                }
                else if(num2.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el segundo numero",Toast.LENGTH_SHORT).show();
                }
                else{
                    float n1 = Float.parseFloat(num1);
                    float n2 = Float.parseFloat(num2);
                    clc.setNum1(n1);
                    clc.setNum2(n2);
                    String resultado = String.valueOf(clc.resta());
                    lblRes.setText(resultado);
                }
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el primer numero",Toast.LENGTH_SHORT).show();
                }
                else if(num2.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el segundo numero",Toast.LENGTH_SHORT).show();
                }
                else{
                    float n1 = Float.parseFloat(num1);
                    float n2 = Float.parseFloat(num2);
                    clc.setNum1(n1);
                    clc.setNum2(n2);
                    String resultado = String.valueOf(clc.multiplicacion());
                    lblRes.setText(resultado);
                }
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();
                if(num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el primer numero",Toast.LENGTH_SHORT).show();
                }
                else if(num2.matches("")){
                    Toast.makeText(CalculadoraActivity.this,"Ingresar el segundo numero",Toast.LENGTH_SHORT).show();
                }
                else{
                    float n1 = Float.parseFloat(num1);
                    float n2 = Float.parseFloat(num2);
                    clc.setNum1(n1);
                    clc.setNum2(n2);
                    String resultado = String.valueOf(clc.division());
                    lblRes.setText(resultado);
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNum1.setText("");
                txtNum2.setText("");
                lblRes.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
