package com.example.recuperacion;

import java.io.Serializable;

public class Calculadora implements Serializable {

    private float num1;
    private float num2;

    public Calculadora(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public Calculadora() {

    }

    public float getNum1() {
        return num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    public float suma(){
        float s = 0;
        s = this.num1 + this.num2;
        return s;
    }

    public float resta(){
        float r = 0;
        r = this.num1 - this.num2;
        return r;
    }

    public float multiplicacion(){
        float m = 0;
        m = this.num1 * this.num2;
        return m;
    }

    public float division(){
        float d = 0;
        d = this.num1 / this.num2;
        return d;
    }

}
